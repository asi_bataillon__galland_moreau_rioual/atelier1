# Atelier1

Introduction au web dynamique sous architecture JEE (Beans, Servlets, JSP, JCBD) 

## Compte rendu de l’atelier 1 

Suite à des problèmes d'ordre matériel concernant les personnes en charge de cette fonctionnalité, l'accès au web service et donc l'affichage des cartes grâce à la technologie 
javascript/AJAX n'a pas pu être développée.
Concernant l'aspect dynamique de notre application, nous nous sommes basés sur une architecture MVC (modèle = Beans, Vue = JSP, Controler = Servlets) qui nous a permis
de séparer la partie traitement avec la partie affichage et de nous rendre justement indépedant d'un quelconque Web Service.
```mermaid
graph TD
    A[Modèle] -->|Notification de changement| B[Vue]
    B -->|Requête d'état| A
    C[Controleur] -->|changement| A
    B -->|Action d'utilisateur| C
    C -->|Choix de vue| B
```
Deux approches ont été développés afin de satisfaire 
deux fonctionnalités voulues : La première concerne le stockage des cartes dans une Base De Données externes (postgresql en l'occurence), la seconde est l'utilisation d'une base de
données stockée en interne du programme afin d'afficher les cartes grâce à l'utilisation d'EL.

Pour ce faire, le travail à été réparti en 4 parties :

__Web statique :__

*Formulaire :*
Il s’agissait de préparer un formulaire html / css / javascript, et d’envoyer les informations à un web service (existant), qui se charge de faire le lien à la base de 
données. La partie purement html/css est fonctionnelle. En revanche comme énoncé précédemment, suite à des soucis liés à éclipse et au serveur tomcat, il nous a été impossible 
de contacter le web service.

![alt text](images/formulaire.png "formulaire")

__Web Dynamique :__

*Affichage :*
Il fallait réaliser la partie affichage de la carte en web dynamique. Nous nous sommes très fortement inspirés du code fournis sur les Poneys. Nous avons maintenant un affichage
fonctionnel. Cependant les images ne s'affichent pas pour une raison encore inconnue et le CSS est largement améliorable. Nous ne sommes également pas sûr de récupérer les 
informations à afficher au bon endroit. Pour l'instant, à l'instar du code Poney, les données sont stockées dans le fichier DAO. Il n'y a donc pas de base de données à proprement parler.

![alt text](images/recherche.png "recherche")


*Connexion à une base externe postgresql :*
Concernant la base de donnée externes, le programme s'occupe de créer la table "Card" mais pas la base de donnée nommée "Cartes" localisée sur le port 8080 usuel
avec les identifiants de connexion "tp","tp". Pour tester l'insertion d'une nouvelle carte, il faudra donc créer cette base de donnée.Les doublons et les erreurs liées
au formulaire ne sont pas gérés par manque de temps.

![alt text](images/bdd.png "bdd")




